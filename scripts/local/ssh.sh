#!/usr/bin/env bash

set -ev

mkdir -p ~/.ssh
touch ~/.ssh/id_rsa
touch ~/.ssh/id_rsa.pub
touch  ~/.ssh/authorized_keys
touch  ~/.ssh/known_hosts
touch  ~/.ssh/config
cp /user/.ssh/id_rsa ~/.ssh/id_rsa
cp /user/.ssh/id_rsa ~/.ssh/id_rsa.pub
chmod 700 ~/.ssh
chmod 644 ~/.ssh/authorized_keys
chmod 644 ~/.ssh/known_hosts
chmod 644 ~/.ssh/config
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub
eval `ssh-agent -s`
ssh-add

set +v
