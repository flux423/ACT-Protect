#!/bin/bash

"eval `ssh-agent -s` && ssh-add /user/.ssh/id_rsa"
touch ~/.bashrc
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash -
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
source ~/.bashrc
nvm install node && nvm install v16.18.1 && nvm use 16.18.1
npm install -g npm@9.1.2

#terminus
rm -rf ~/.terminus/plugins/terminus-aliases-plugin
mkdir -p ~/.terminus/plugins
git clone git@github.com:pantheon-systems/terminus-aliases-plugin.git ~/.terminus/plugins/terminus-aliases-plugin
terminus auth:login --machine-token=loheWAqSeQGFDLyK9_U0o0dFnGW7xq1csNfD-coVL4pZB
composer update --no-dev -n -o -d ~/.terminus/plugins/terminus-aliases-plugin
npm install
npm run node-scss
source ~/.bashrc
