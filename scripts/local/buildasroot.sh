#!/bin/bash

touch /usr/local/etc/php/conf.d/docker-php-ext-ci.ini
echo 'memory_limit = -1' >> /usr/local/etc/php/conf.d/docker-php-ext-ci.ini

apt-get update --fix-missing && apt-get -y upgrade
apt-get install -y build-essential libssl-dev git sudo unzip gcc g++ make chromium nano
curl -sL https://deb.nodesource.com/setup_16.x | bash -
apt-get install -y nodejs
chown -R www-data /usr/lib/node_modules
chown -R www-data /usr/bin
npm install -g npm@9.1.2
