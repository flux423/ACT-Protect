<?php

/**
 * @file
 * Local development override configuration feature.
 */

use Drupal\Component\Assertion\Handle;

/**
 * Database configuration.
 */
$databases = [
  'default' =>
    [
      'default' =>
        [
          'database' => 'pantheon',
          'username' => 'pantheon',
          'password' => 'pantheon',
          'host' => 'database',
          'port' => '3306',
          'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
          'driver' => 'mysql',
          'prefix' => '',
        ],
    ],
];

$dir = dirname(DRUPAL_ROOT);
assert_options(ASSERT_ACTIVE, TRUE);
Handle::register();

$settings['container_yamls'][] = $dir . '/docroot/sites/development.services.yml';
$settings['container_yamls'][] = $dir . '/docroot/sites/blt.development.services.yml';
$settings['update_free_access'] = TRUE;
$config['system.logging']['error_level'] = 'verbose';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$settings['extension_discovery_scan_tests'] = FALSE;
$settings['rebuild_access'] = FALSE;
$settings['skip_permissions_hardening'] = TRUE;
$config['system.file']['path']['temporary'] = '/tmp';
$settings['file_private_path'] = $dir . '/files-private';
$settings['trusted_host_patterns'] = [
  '^.+$',
];
